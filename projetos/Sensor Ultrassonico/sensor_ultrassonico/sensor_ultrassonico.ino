#include <Ultrasonic.h>


/*
  Sensor Ultrassônico - Medir a distância
*/

/* Definições */
#define US_TRIGGER_PIN      21    // Pino ...
#define US_ECHO_PIN         12
#define US_QTY_ECHO_PINS    1     // 

/* Variáveis globais */
Ultrasonic ultrasonic(US_TRIGGER_PIN, US_ECHO_PIN);
int distancia = 0;
char message[30] = "";

// the setup function runs once when you press reset or power the board
void setup() {
  // Configuração da serial
  Serial.begin(9600);
  Serial.println("Inicializando.....");

  // Configuração dos pinos do sensor Ultrassônico
  pinMode(US_ECHO_PIN, INPUT);
  pinMode(US_TRIGGER_PIN, OUTPUT);
  
  // Configuração do pino de LED
  pinMode(LED_BUILTIN, OUTPUT);
}

// Funcao loop para executar sempre
void loop() {
  
  Serial.println("Reading US sensor....");
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  
  // Faz a leitura da distancia
  hcsr04();
  
  sprintf(message, "Distancia=%d cm zrzn", distancia);
  Serial.write(message, strlen(message));
  
  Serial.println("Finalizado");
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}


// Funcao responsavel por fazer a leitura da distancia
void hcsr04(){
  
  digitalWrite(US_TRIGGER_PIN, LOW);  // Coloca o pino TRIGGER em nivel baixo
  delayMicroseconds(2);               // Aguarda 2 microssengundos
  digitalWrite(US_TRIGGER_PIN, HIGH); // Coloca o pino TRIGGER em nivel alto
  delayMicroseconds(10);              // Aguarda 10 microssegundos
  digitalWrite(US_TRIGGER_PIN, LOW);  // Coloca o pino TRIGGER em nivel baixo novamente
  
  // Funcao RANGING, que faz a conversao do tempo de resposta do echo retornando o valor em centimetros
  distancia = (ultrasonic.Ranging(CM)); 
  
  delay(500); // Aguarda 500 milissegundos
}
