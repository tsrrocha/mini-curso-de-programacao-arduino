# Pisca LED

Projeto inicial para demonstração e primeiro contato com o Arduino. Será utilizada a placa ATMega 2560.

## Hardware

Projeto de hardware ...

## Software

Projeto de software ...

#### Sequência do software



```plantuml
title Sequência do software do projeto PiscaLED

partition "Setup Method" {
	(*) --> "Configura D13 como Saída digital"
	note right: O pino D13 é conectado internamente ao LED da placa ATMega 2560
	"Configura D13 como Saída digital" --> "Coloca D13 em nível baixo (0 Vdc ou LOW)"
	"Coloca D13 em nível baixo (0 Vdc ou LOW)" --> "Configura a Serial com Baudrate em 115200"
	"Configura a Serial com Baudrate em 115200" -> "First Action"
	"First Action" --> "Second Action"
	
}

partition "Loop Method" {
	"Second Action" --> "Ativa LED"
	"Ativa LED" --> "Aguarda 0.5 segundo"
	"Aguarda 0.5 segundo" --> "Desativa o LED"
	"Desativa o LED" --> "Aguarda 1 Segundo"
	"Aguarda 1 Segundo" --> "Ativa LED"
}



```

